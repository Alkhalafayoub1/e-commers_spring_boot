package thkoeln.archilab.ecommerce.regression;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import thkoeln.archilab.ecommerce.TestHelper;
import thkoeln.archilab.ecommerce.usecases.*;

import javax.transaction.Transactional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static thkoeln.archilab.ecommerce.TestHelper.USER_DATA;
import static thkoeln.archilab.ecommerce.TestHelper.InvalidReason.*;
import static thkoeln.archilab.ecommerce.TestHelper.ITEM_DATA;

@SpringBootTest
@Transactional
public class RegressionItemCatalogTest {

    @Autowired
    private ShoppingBasketUseCases shoppingBasketUseCases;
    @Autowired
    private ItemCatalogUseCases itemCatalogUseCases;
    @Autowired
    private InventoryManagementUseCases inventoryManagementUseCases;
    @Autowired
    private TestHelper testHelper;

    @BeforeEach
    public void setUp() {
        shoppingBasketUseCases.deleteAllOrders();
        itemCatalogUseCases.deleteItemCatalog();
    }



    @Test
    public void testAddItemToCatalog() {
        // given
        testHelper.addAllItems();

        // when
        Float salesPrice = itemCatalogUseCases.getSalesPrice( (UUID) ITEM_DATA[4][0] );

        // then
        assertEquals( ITEM_DATA[4][5], salesPrice );
    }


    @Test
    public void testAddItemWithInvalidData() {
        // given
        Object[][] invalidItemData = {
                testHelper.getItemDataInvalidAtIndex( 0, NULL ),     // id
                testHelper.getItemDataInvalidAtIndex( 1, NULL ),     // name
                testHelper.getItemDataInvalidAtIndex( 1, EMPTY ),    // name
                testHelper.getItemDataInvalidAtIndex( 2, NULL ),     // description
                testHelper.getItemDataInvalidAtIndex( 2, EMPTY ),    // description
                testHelper.getItemDataInvalidAtIndex( 4, NULL ),     // purchasePrice
                testHelper.getItemDataInvalidAtIndex( 4, ZERO ),     // purchasePrice
                testHelper.getItemDataInvalidAtIndex( 5, NULL ),     // salesPrice
                testHelper.getItemDataInvalidAtIndex( 5, ZERO ),     // purchasePrice
                testHelper.getItemDataInvalidAtIndex( 5, TOO_LOW )   // salesPrice
        };

        // when
        // then
        for ( Object[] invalidItemDataItem : invalidItemData ) {
            StringBuffer invalidDataString = new StringBuffer().append( invalidItemDataItem[0] )
                .append( ", " ).append( invalidItemDataItem[1] ).append( ", " ).append( invalidItemDataItem[2] )
                .append( ", " ).append( invalidItemDataItem[3] ).append( ", " ).append( invalidItemDataItem[4] )
                .append( ", " ).append( invalidItemDataItem[5] );
            assertThrows( ShopException.class, () -> testHelper.addItemDataToCatalog( invalidItemDataItem ),
                          "Invalid data: " + invalidDataString.toString() );
        }
    }


    @Test
    public void testRemoveItemFromCatalog() {
        // given
        testHelper.addAllItems();
        UUID itemId = (UUID) ITEM_DATA[4][0];

        // when
        assertDoesNotThrow( () -> itemCatalogUseCases.getSalesPrice( itemId ) );
        itemCatalogUseCases.removeItemFromCatalog( itemId );

        // then
        assertThrows( ShopException.class, () -> itemCatalogUseCases.getSalesPrice( itemId ) );
    }



    @Test
    public void testRemoveNonExistentItem() {
        // given
        testHelper.addAllItems();
        UUID nonExistentItemId = UUID.randomUUID();

        // when
        // then
        assertThrows( ShopException.class, () -> itemCatalogUseCases.removeItemFromCatalog( nonExistentItemId ) );
    }



    @Test
    public void testRemoveItemThatIsInInventory() {
        // given
        testHelper.addAllItems();
        UUID itemId = (UUID) ITEM_DATA[4][0];
        inventoryManagementUseCases.addToInventory( itemId, 3 );

        // when
        // then
        assertThrows( ShopException.class, () -> itemCatalogUseCases.removeItemFromCatalog( itemId ) );
    }


    @Test
    public void testRemoveItemThatIsInShoppingBasketOrOrder() {
        // given
        testHelper.addAllItems();
        testHelper.registerAllUsers();
        UUID itemId3 = (UUID) ITEM_DATA[3][0];
        UUID itemId4 = (UUID) ITEM_DATA[4][0];
        String userMailAddress3 = USER_DATA[3][1];
        String userMailAddress4 = USER_DATA[4][1];
        inventoryManagementUseCases.addToInventory( itemId3, 3 );
        inventoryManagementUseCases.addToInventory( itemId4, 4 );

        // when
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId3, 3 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress4, itemId4, 4 );
        shoppingBasketUseCases.checkout( userMailAddress4 );

        // then
        assertThrows( ShopException.class, () -> itemCatalogUseCases.removeItemFromCatalog( itemId3 ) );
        assertThrows( ShopException.class, () -> itemCatalogUseCases.removeItemFromCatalog( itemId4 ) );
    }


    @Test
    public void testClearItemCatalog() {
        // given
        testHelper.addAllItems();

        // when
        itemCatalogUseCases.deleteItemCatalog();

        // then
        assertThrows( ShopException.class, () -> itemCatalogUseCases.getSalesPrice( (UUID) ITEM_DATA[4][0] ) );
    }

}
