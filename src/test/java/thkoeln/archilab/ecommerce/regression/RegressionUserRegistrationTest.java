package thkoeln.archilab.ecommerce.regression;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import thkoeln.archilab.ecommerce.TestHelper;
import thkoeln.archilab.ecommerce.usecases.*;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;
import static thkoeln.archilab.ecommerce.TestHelper.InvalidReason.*;

@SpringBootTest
@Transactional
public class RegressionUserRegistrationTest {

    @Autowired
    private UserRegistrationUseCases userRegistrationUseCases;
    @Autowired
    private ShoppingBasketUseCases shoppingBasketUseCases;
    @Autowired
    private TestHelper testHelper;

    @BeforeEach
    public void setUp() {
        shoppingBasketUseCases.deleteAllOrders();
        userRegistrationUseCases.deleteAllUsers();
    }


    @Test
    public void testAllUsersRegistered() {
        // given
        testHelper.registerAllUsers();

        // when
        String[] user3 = userRegistrationUseCases.getUserData( TestHelper.USER_DATA[3][1] );

        // then
        Assertions.assertArrayEquals( TestHelper.USER_DATA[3], user3 );
    }


    @Test
    public void testRegisterUserWithDuplicateMailAddress() {
        // given
        testHelper.registerAllUsers();

        // when
        // then
        assertThrows( ShopException.class, () ->
                userRegistrationUseCases.register( "Xxx Yyy", TestHelper.USER_DATA[5][1], "Second St", "Aachen", "90001" ) );
    }


    @Test
    public void testRegisterUserWithInvalidData() {
        for ( int i = 0; i < 5; i++ ) {
            // given
            String[] invalidUserDataEmpty = testHelper.getUserDataInvalidAtIndex( i, EMPTY );
            String[] invalidUserDataNull = testHelper.getUserDataInvalidAtIndex( i, NULL );

            // when
            // then
            assertThrows( ShopException.class, () -> testHelper.registerUserData( invalidUserDataEmpty ) );
            assertThrows( ShopException.class, () -> testHelper.registerUserData( invalidUserDataNull ) );
        }
    }


    @Test
    public void testChangeAddressSuccessful() {
        // given
        testHelper.registerAllUsers();

        // when
        userRegistrationUseCases.changeAddress( TestHelper.USER_DATA[6][1], "New Street", "New City", "90002" );
        String[] newUser6 = userRegistrationUseCases.getUserData( TestHelper.USER_DATA[6][1] );

        // then
        Assertions.assertEquals( TestHelper.USER_DATA[6][0], newUser6[0] );
        Assertions.assertEquals( TestHelper.USER_DATA[6][1], newUser6[1] );
        assertEquals( "New Street", newUser6[2] );
        assertEquals( "New City", newUser6[3] );
        assertEquals( "90002", newUser6[4] );
    }



    @Test
    public void testChangeAddressForNonexistingMailAddress() {
        // given
        testHelper.registerAllUsers();

        // when
        // then
        assertThrows( ShopException.class, () ->
                userRegistrationUseCases.changeAddress( "nonexisting@nowhere.de", "New Street", "New City", "90002" ) );
    }


    @Test
    public void testChangeAddressWithInvalidData() {
        // given
        testHelper.registerAllUsers();

        // when
        // then
        // start replace with 2, since 0 and 1 are name and mailAddress, this should remain valid
        for ( int i = 2; i < 5; i++ ) {
            String[] invalidAddressDataEmpty = testHelper.getUserDataInvalidAtIndex( i, EMPTY );
            String[] invalidAddressDataNull = testHelper.getUserDataInvalidAtIndex( i, NULL );

            // when
            // then
            assertThrows( ShopException.class, () ->
                    userRegistrationUseCases.changeAddress( invalidAddressDataEmpty[1],
                            invalidAddressDataEmpty[2], invalidAddressDataEmpty[3], invalidAddressDataEmpty[4] ) );
            assertThrows( ShopException.class, () ->
                    userRegistrationUseCases.changeAddress( invalidAddressDataNull[1],
                            invalidAddressDataNull[2], invalidAddressDataNull[3], invalidAddressDataNull[4] ) );
        }
    }


    @Test
    public void testGetDataForNonexistingMailAddress() {
        // given
        testHelper.registerAllUsers();

        // when
        // then
        assertThrows( ShopException.class, () ->
                userRegistrationUseCases.getUserData( "nonexisting@nowhere.de" ) );
    }


    @Test
    public void testDeleteUsersNoMoreUsers() {
        // given
        testHelper.registerAllUsers();

        // when
        userRegistrationUseCases.deleteAllUsers();

        // then
        assertThrows( ShopException.class, () -> userRegistrationUseCases.getUserData( TestHelper.USER_DATA[0][1] ) );
    }

}
