package thkoeln.archilab.ecommerce.regression;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import thkoeln.archilab.ecommerce.TestHelper;
import thkoeln.archilab.ecommerce.usecases.*;

import javax.transaction.Transactional;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static thkoeln.archilab.ecommerce.TestHelper.*;

@SpringBootTest
@Transactional
public class RegressionShoppingBasketTest {

    @Autowired
    private UserRegistrationUseCases userRegistrationUseCases;
    @Autowired
    private ShoppingBasketUseCases shoppingBasketUseCases;
    @Autowired
    private ItemCatalogUseCases itemCatalogUseCases;
    @Autowired
    private InventoryManagementUseCases inventoryManagementUseCases;
    @Autowired
    private TestHelper testHelper;

    @BeforeEach
    public void setUp() {
        shoppingBasketUseCases.deleteAllOrders();
        userRegistrationUseCases.deleteAllUsers();
        itemCatalogUseCases.deleteItemCatalog();
        testHelper.registerAllUsers();
        testHelper.addAllItems();
        testHelper.inventoryUpAllItems();
    }


    @Test
    public void testInvalidAddToShoppingBasket() {
        // given
        UUID nonExistentItemId = UUID.randomUUID();
        String nonExistingMailAddress = "this@no.nonono";
        UUID itemId5 = (UUID) ITEM_DATA[5][0];
        UUID itemId0 = (UUID) ITEM_DATA[0][0];
        UUID itemId1 = (UUID) ITEM_DATA[1][0];
        UUID itemId2 = (UUID) ITEM_DATA[2][0];
        String userMailAddress0 = TestHelper.USER_DATA[0][1];

        // when
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress0, itemId2, 6 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress0, itemId2, 13 );

        // then
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress0, nonExistentItemId, 12 ) );
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.addItemToShoppingBasket( nonExistingMailAddress, itemId5, 12 ) );
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress0, itemId5, -1 ) );
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress0, itemId0, 1 ) );
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress0, itemId1, 11 ) );
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress0, itemId2, 2 ) );
    }



    @Test
    public void testInvalidRemoveFromShoppingBasket() {
        // given
        UUID nonExistentItemId = UUID.randomUUID();
        String nonExistingMailAddress = "this@no.nonono";
        UUID itemId5 = (UUID) ITEM_DATA[5][0];
        UUID itemId0 = (UUID) ITEM_DATA[0][0];
        UUID itemId1 = (UUID) ITEM_DATA[1][0];
        UUID itemId2 = (UUID) ITEM_DATA[2][0];
        String userMailAddress0 = TestHelper.USER_DATA[0][1];
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress0, itemId1, 5 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress0, itemId2, 15 );

        // when
        shoppingBasketUseCases.removeItemFromShoppingBasket( userMailAddress0, itemId1, 2 );
        shoppingBasketUseCases.removeItemFromShoppingBasket( userMailAddress0, itemId2, 4 );
        shoppingBasketUseCases.removeItemFromShoppingBasket( userMailAddress0, itemId2, 7 );

        // then
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.removeItemFromShoppingBasket( userMailAddress0, nonExistentItemId, 12 ) );
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.removeItemFromShoppingBasket( nonExistingMailAddress, itemId5, 12 ) );
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.removeItemFromShoppingBasket( userMailAddress0, itemId5, -1 ) );
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.removeItemFromShoppingBasket( userMailAddress0, itemId0, 1 ) );
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.removeItemFromShoppingBasket( userMailAddress0, itemId1, 4 ) );
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.removeItemFromShoppingBasket( userMailAddress0, itemId2, 5 ) );
    }



    @Test
    public void testAddRemoveItemsFromAndToShoppingBasket() {
        // given
        UUID itemId1 = (UUID) TestHelper.ITEM_DATA[1][0];
        UUID itemId2 = (UUID) TestHelper.ITEM_DATA[2][0];
        String userMailAddress3 = TestHelper.USER_DATA[3][1];
        String userMailAddress5 = TestHelper.USER_DATA[5][1];

        // when
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId1, 2 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId2, 3 );
        shoppingBasketUseCases.removeItemFromShoppingBasket( userMailAddress3, itemId1, 1 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId1, 0 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId2, 6 );
        // user3 has 1x itemId1 and 9x itemId2 in cart
        Map<UUID, Integer> cart3 = shoppingBasketUseCases.getShoppingBasketAsMap( userMailAddress3 );

        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress5, itemId1, 2 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress5, itemId2, 8 );
        shoppingBasketUseCases.removeItemFromShoppingBasket( userMailAddress5, itemId1, 1 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress5, itemId2, 2 );
        // user5 has 1x itemId1 and 10x itemId2 in cart
        Map<UUID, Integer> cart5 = shoppingBasketUseCases.getShoppingBasketAsMap( userMailAddress5 );
        int reservedInventory1 = shoppingBasketUseCases.getReservedInventoryInShoppingBaskets( itemId1 );
        int reservedInventory2 = shoppingBasketUseCases.getReservedInventoryInShoppingBaskets( itemId2 );

        // then
        assertEquals( 2, cart3.size() );
        assertEquals( 1, cart3.get( itemId1 ) );
        assertEquals( 9, cart3.get( itemId2 ) );

        assertEquals( 2, cart5.size() );
        assertEquals( 1, cart5.get( itemId1 ) );
        assertEquals( 10, cart5.get( itemId2 ) );

        assertEquals( 2, reservedInventory1 );
        assertEquals( 19, reservedInventory2 );
    }


    @Test
    public void testImpactOfInventoryCorrectionToOneShoppingBasket() {
        // given
        UUID itemId1 = (UUID) TestHelper.ITEM_DATA[1][0];
        UUID itemId2 = (UUID) TestHelper.ITEM_DATA[2][0];
        UUID itemId3 = (UUID) TestHelper.ITEM_DATA[3][0];
        String userMailAddress3 = TestHelper.USER_DATA[3][1];

        // when
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId1, 6 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId2, 15 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId3, 1 );
        inventoryManagementUseCases.changeInventoryTo( itemId1, 4 );
        inventoryManagementUseCases.changeInventoryTo( itemId2, 16 );
        inventoryManagementUseCases.changeInventoryTo( itemId3, 0 );
        Map<UUID, Integer> cart = shoppingBasketUseCases.getShoppingBasketAsMap( userMailAddress3 );
        int reservedInventory1 = shoppingBasketUseCases.getReservedInventoryInShoppingBaskets( itemId1 );
        int reservedInventory2 = shoppingBasketUseCases.getReservedInventoryInShoppingBaskets( itemId2 );
        int reservedInventory3 = shoppingBasketUseCases.getReservedInventoryInShoppingBaskets( itemId3 );

        // then
        assertEquals( 4, cart.get( itemId1 ) );
        assertEquals( 15, cart.get( itemId2 ) );
        assertTrue( cart.get( itemId3 ) == null || cart.get( itemId3 ) == 0 );
        assertEquals( 4, reservedInventory1 );
        assertEquals( 15, reservedInventory2 );
        assertEquals( 0, reservedInventory3 );
    }


    @Test
    public void testImpactOfInventoryCorrectionToSeveralShoppingBaskets() {
        // given
        UUID itemId2 = (UUID) TestHelper.ITEM_DATA[2][0];
        String userMailAddress3 = TestHelper.USER_DATA[3][1];
        String userMailAddress6 = TestHelper.USER_DATA[6][1];
        String userMailAddress9 = TestHelper.USER_DATA[9][1];

        // when
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId2, 3 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress6, itemId2, 6 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress9, itemId2, 9 );
        inventoryManagementUseCases.removeFromInventory( itemId2, 2 );
        Map<UUID, Integer> cart31 = shoppingBasketUseCases.getShoppingBasketAsMap( userMailAddress3 );
        Map<UUID, Integer> cart61 = shoppingBasketUseCases.getShoppingBasketAsMap( userMailAddress6 );
        Map<UUID, Integer> cart91 = shoppingBasketUseCases.getShoppingBasketAsMap( userMailAddress9 );
        int reservedInventory21 = shoppingBasketUseCases.getReservedInventoryInShoppingBaskets( itemId2 );

        inventoryManagementUseCases.removeFromInventory( itemId2, 8 );
        Map<UUID, Integer> cart32 = shoppingBasketUseCases.getShoppingBasketAsMap( userMailAddress3 );
        Map<UUID, Integer> cart62 = shoppingBasketUseCases.getShoppingBasketAsMap( userMailAddress6 );
        Map<UUID, Integer> cart92 = shoppingBasketUseCases.getShoppingBasketAsMap( userMailAddress9 );
        int quantity32 = cart32.get( itemId2 ) == null ? 0 : cart32.get( itemId2 );
        int quantity62 = cart62.get( itemId2 ) == null ? 0 : cart62.get( itemId2 );
        int quantity92 = cart92.get( itemId2 ) == null ? 0 : cart92.get( itemId2 );
        int reservedInventory22 = shoppingBasketUseCases.getReservedInventoryInShoppingBaskets( itemId2 );

        // then
        assertEquals( 3, cart31.get( itemId2 ) );
        assertEquals( 6, cart61.get( itemId2 ) );
        assertEquals( 9, cart91.get( itemId2 ) );
        assertEquals( 18, reservedInventory21 );
        assertEquals( 10, reservedInventory22 );
        assertEquals( reservedInventory22, quantity32 + quantity62 + quantity92 );
    }


    @Test
    public void testShoppingBasketValue() {
        // given
        UUID itemId3 = (UUID) TestHelper.ITEM_DATA[3][0];
        UUID itemId6 = (UUID) TestHelper.ITEM_DATA[6][0];
        UUID itemId8 = (UUID) TestHelper.ITEM_DATA[8][0];
        Float price3 = (Float) TestHelper.ITEM_DATA[3][5];
        Float price6 = (Float) TestHelper.ITEM_DATA[6][5];
        Float price8 = (Float) TestHelper.ITEM_DATA[8][5];
        String userMailAddress3 = TestHelper.USER_DATA[3][1];

        // when
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId3, 3 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId6, 2 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId8, 5 );
        // user3 has 3x itemId3, 2x itemId6 and 5x itemId8 in cart
        float cartValue = shoppingBasketUseCases.getShoppingBasketAsMoneyValue( userMailAddress3 );

        // then
        assertEquals( 3 * price3 + 2 * price6 + 5 * price8, cartValue, 0.1f );
    }


    @Test
    public void testShoppingBasketValueInvalid() {
        // given
        String nonExistingMailAddress = "this@no.never";

        // when
        // then
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.getShoppingBasketAsMoneyValue( nonExistingMailAddress ) );
    }


    @Test
    public void testCheckout() {
        // given
        UUID itemId1 = (UUID) TestHelper.ITEM_DATA[1][0];
        UUID itemId2 = (UUID) TestHelper.ITEM_DATA[2][0];
        UUID itemId3 = (UUID) TestHelper.ITEM_DATA[3][0];
        int inventory1before = ITEM_INVENTORY.get( itemId1 );
        int inventory2before = ITEM_INVENTORY.get( itemId2 );
        int inventory3before = ITEM_INVENTORY.get( itemId3 );
        String userMailAddress3 = TestHelper.USER_DATA[3][1];
        Map<UUID, Integer> orderHistoryBefore = shoppingBasketUseCases.getOrderHistory( userMailAddress3 );

        // when
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId1, inventory1before-2 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId2, 4 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress3, itemId3, 5 );
        shoppingBasketUseCases.checkout( userMailAddress3 );
        int inventory1after = inventoryManagementUseCases.getAvailableInventory( itemId1 );
        int inventory2after = inventoryManagementUseCases.getAvailableInventory( itemId2 );
        int inventory3after = inventoryManagementUseCases.getAvailableInventory( itemId3 );
        Map<UUID, Integer> orderHistoryAfter = shoppingBasketUseCases.getOrderHistory( userMailAddress3 );

        // then
        assertEquals( 0, orderHistoryBefore.size() );
        assertEquals( 2, inventory1after );
        assertEquals( inventory2before-4, inventory2after );
        assertEquals( inventory3before-5, inventory3after );
        assertEquals( 3, orderHistoryAfter.size() );
        assertEquals( inventory1before-2, orderHistoryAfter.get( itemId1 ) );
        assertEquals( 4, orderHistoryAfter.get( itemId2 ) );
        assertEquals( 5, orderHistoryAfter.get( itemId3 ) );
    }


    @Test
    public void testCheckoutInvalid() {
        // given
        String nonExistingMailAddress = "this@no.nono";
        String userMailAddress3 = TestHelper.USER_DATA[3][1];
        String userMailAddress5 = TestHelper.USER_DATA[5][1];
        UUID itemId2 = (UUID) TestHelper.ITEM_DATA[2][0];

        // when
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress5, itemId2, 4 );
        shoppingBasketUseCases.removeItemFromShoppingBasket( userMailAddress5, itemId2, 4 );

        // then
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.checkout( nonExistingMailAddress ) );
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.checkout( userMailAddress3 ) );
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.checkout( userMailAddress5 ) );
    }


    @Test
    public void testOrderHistory() {
        // given
        UUID itemId1 = (UUID) TestHelper.ITEM_DATA[1][0];
        UUID itemId2 = (UUID) TestHelper.ITEM_DATA[2][0];
        String userMailAddress = TestHelper.USER_DATA[7][1];
        Map<UUID, Integer> orderHistoryBefore = shoppingBasketUseCases.getOrderHistory( userMailAddress );

        // when
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress, itemId1, 3 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress, itemId2, 2 );
        shoppingBasketUseCases.checkout( userMailAddress );
        Map<UUID, Integer> orderHistory1 = shoppingBasketUseCases.getOrderHistory( userMailAddress );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress, itemId1, 6 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress, itemId2, 2 );
        shoppingBasketUseCases.checkout( userMailAddress );
        Map<UUID, Integer> orderHistory2 = shoppingBasketUseCases.getOrderHistory( userMailAddress );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress, itemId1, 1 );
        shoppingBasketUseCases.addItemToShoppingBasket( userMailAddress, itemId2, 6 );
        shoppingBasketUseCases.checkout( userMailAddress );
        Map<UUID, Integer> orderHistory3 = shoppingBasketUseCases.getOrderHistory( userMailAddress );

        // then
        assertEquals( 0, orderHistoryBefore.size() );
        assertEquals( 2, orderHistory1.size() );
        assertEquals( 2, orderHistory2.size() );
        assertEquals( 2, orderHistory3.size() );
        assertEquals( 3, orderHistory1.get( itemId1 ) );
        assertEquals( 2, orderHistory1.get( itemId2 ) );
        assertEquals( 9, orderHistory2.get( itemId1 ) );
        assertEquals( 4, orderHistory2.get( itemId2 ) );
        assertEquals( 10, orderHistory3.get( itemId1 ) );
        assertEquals( 10, orderHistory3.get( itemId2 ) );
    }


}
