package thkoeln.archilab.ecommerce.regression;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import thkoeln.archilab.ecommerce.TestHelper;
import thkoeln.archilab.ecommerce.usecases.*;

import javax.transaction.Transactional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static thkoeln.archilab.ecommerce.TestHelper.ITEM_DATA;
import static thkoeln.archilab.ecommerce.TestHelper.ITEM_INVENTORY;

@SpringBootTest
@Transactional
public class RegressionInventoryManagementTest {

    @Autowired
    private UserRegistrationUseCases userRegistrationUseCases;
    @Autowired
    private ShoppingBasketUseCases shoppingBasketUseCases;
    @Autowired
    private ItemCatalogUseCases itemCatalogUseCases;
    @Autowired
    private InventoryManagementUseCases inventoryManagementUseCases;
    @Autowired
    private TestHelper testHelper;


    @BeforeEach
    public void setUp() {
        shoppingBasketUseCases.deleteAllOrders();
        userRegistrationUseCases.deleteAllUsers();
        itemCatalogUseCases.deleteItemCatalog();
        testHelper.addAllItems();
    }


    @Test
    public void testAddToInventory() {
        // given
        testHelper.inventoryUpAllItems();
        UUID itemId8 = (UUID) ITEM_DATA[8][0];

        // when
        int inventory8before = inventoryManagementUseCases.getAvailableInventory( itemId8 );
        assertEquals( ITEM_INVENTORY.get( itemId8 ), inventory8before );
        inventoryManagementUseCases.addToInventory( itemId8, 23 );
        int inventory8after = inventoryManagementUseCases.getAvailableInventory( itemId8 );
        inventoryManagementUseCases.addToInventory( itemId8, 0 );
        int inventory8after2 = inventoryManagementUseCases.getAvailableInventory( itemId8 );

        // then
        assertEquals( inventory8before + 23, inventory8after );
        assertEquals( inventory8after, inventory8after2 );
    }



    @Test
    public void testInvalidAddToInventory() {
        // given
        testHelper.inventoryUpAllItems();
        UUID nonExistentItemId = UUID.randomUUID();
        UUID itemId2 = (UUID) ITEM_DATA[2][0];

        // when
        // then
        assertThrows( ShopException.class, () -> inventoryManagementUseCases.addToInventory( nonExistentItemId, 12 ) );
        assertThrows( ShopException.class, () -> inventoryManagementUseCases.addToInventory( itemId2, -1 ) );
    }


    @Test
    public void testRemoveFromInventory() {
        // given
        testHelper.inventoryUpAllItems();
        UUID itemId6 = (UUID) ITEM_DATA[6][0];
        int inventory6before = ITEM_INVENTORY.get( itemId6 );
        UUID itemId9 = (UUID) ITEM_DATA[9][0];
        int inventory9before = ITEM_INVENTORY.get( itemId9 );
        UUID itemId0 = (UUID) ITEM_DATA[0][0];

        // when
        inventoryManagementUseCases.removeFromInventory( itemId6, 1 );
        int inventory6after = inventoryManagementUseCases.getAvailableInventory( itemId6 );
        inventoryManagementUseCases.removeFromInventory( itemId0, 0 );
        int inventory0after = inventoryManagementUseCases.getAvailableInventory( itemId0 );
        inventoryManagementUseCases.removeFromInventory( itemId9, inventory9before );
        int inventory9after = inventoryManagementUseCases.getAvailableInventory( itemId0 );

        // then
        assertEquals( inventory6before - 1, inventory6after );
        assertEquals( 0, inventory0after );
        assertEquals( 0, inventory9after );
    }


    @Test
    public void testInvalidRemoveFromInventory() {
        // given
        testHelper.inventoryUpAllItems();
        UUID nonExistentItemId = UUID.randomUUID();
        UUID itemId5 = (UUID) ITEM_DATA[5][0];
        int inventory5before = ITEM_INVENTORY.get( itemId5 );
        UUID itemId0 = (UUID) ITEM_DATA[0][0];

        // when
        // then
        assertThrows( ShopException.class, () -> inventoryManagementUseCases.removeFromInventory( nonExistentItemId, 12 ) );
        assertThrows( ShopException.class, () -> inventoryManagementUseCases.removeFromInventory( itemId5, -1 ) );
        assertThrows( ShopException.class, () -> inventoryManagementUseCases.removeFromInventory( itemId5, inventory5before+1 ) );
        assertThrows( ShopException.class, () -> inventoryManagementUseCases.removeFromInventory( itemId0, 1 ) );
    }


    @Test
    public void testChangeInventory() {
        // given
        testHelper.inventoryUpAllItems();
        UUID itemId10 = (UUID) ITEM_DATA[10][0];

        // when
        inventoryManagementUseCases.changeInventoryTo( itemId10, 111 );
        int inventory10after = inventoryManagementUseCases.getAvailableInventory( itemId10 );

        // then
        assertEquals( 111, inventory10after );
    }


    @Test
    public void testInvalidChangeInventory() {
        // given
        testHelper.inventoryUpAllItems();
        UUID nonExistentItemId = UUID.randomUUID();
        UUID itemId11 = (UUID) ITEM_DATA[11][0];

        // when
        // then
        assertThrows( ShopException.class, () -> inventoryManagementUseCases.changeInventoryTo( nonExistentItemId, 12 ) );
        assertThrows( ShopException.class, () -> inventoryManagementUseCases.changeInventoryTo( itemId11, -1 ) );
    }

}
