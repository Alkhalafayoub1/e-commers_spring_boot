package thkoeln.archilab.ecommerce.e2tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import thkoeln.archilab.ecommerce.TestHelper;
import thkoeln.archilab.ecommerce.usecases.*;

import javax.transaction.Transactional;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
public class E2CheckoutTest {

    @Autowired
    private UserRegistrationUseCases userRegistrationUseCases;
    @Autowired
    private ShoppingBasketUseCases shoppingBasketUseCases;
    @Autowired
    private ItemCatalogUseCases itemCatalogUseCases;
    @Autowired
    private InventoryManagementUseCases inventoryManagementUseCases;
    @Autowired
    private TestHelper testHelper;

    @BeforeEach
    public void setUp() {
        shoppingBasketUseCases.deleteAllOrders();
        userRegistrationUseCases.deleteAllUsers();
        itemCatalogUseCases.deleteItemCatalog();
        testHelper.registerAllUsers();
        testHelper.addAllItems();
        testHelper.inventoryUpAllItems();
    }


    @Test
    public void testShoppingBasketPaymentLimits() {
        // given
        UUID itemId100EUR = (UUID) TestHelper.ITEM_DATA[13][0];
        UUID itemId25EUR = (UUID) TestHelper.ITEM_DATA[12][0];
        UUID itemId1EUR = (UUID) TestHelper.ITEM_DATA[4][0];
        String mailAddress3 = TestHelper.USER_DATA[3][1];
        assertTrue( shoppingBasketUseCases.isEmpty( mailAddress3 ) );
        assertFalse( shoppingBasketUseCases.isPaymentAuthorized( mailAddress3 ) );

        // when
        shoppingBasketUseCases.addItemToShoppingBasket( mailAddress3, itemId100EUR, 4 );
        shoppingBasketUseCases.addItemToShoppingBasket( mailAddress3, itemId25EUR, 4 );
        shoppingBasketUseCases.addItemToShoppingBasket( mailAddress3, itemId1EUR, 1 );
        assertFalse( shoppingBasketUseCases.isEmpty( mailAddress3 ) );
        assertEquals( 501.0f, shoppingBasketUseCases.getShoppingBasketAsMoneyValue( mailAddress3 ), 0.01f );
        assertFalse( shoppingBasketUseCases.isPaymentAuthorized( mailAddress3 ) );

        // then
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.checkout( mailAddress3 ) );
        assertFalse( shoppingBasketUseCases.isPaymentAuthorized( mailAddress3 ) );
        assertFalse( shoppingBasketUseCases.isEmpty( mailAddress3 ) );
        assertFalse( shoppingBasketUseCases.isPaymentAuthorized( mailAddress3 ) );
        Map<UUID, Integer> orderHistory = shoppingBasketUseCases.getOrderHistory( mailAddress3 );
        assertEquals( 0, orderHistory.size() );

        // and then remove 1 EUR and it should be ok
        shoppingBasketUseCases.removeItemFromShoppingBasket( mailAddress3, itemId1EUR, 1 );
        assertEquals( 500.0f, shoppingBasketUseCases.getShoppingBasketAsMoneyValue( mailAddress3 ), 0.01f );
        assertDoesNotThrow( () -> shoppingBasketUseCases.checkout( mailAddress3 ) );
        assertTrue( shoppingBasketUseCases.isEmpty( mailAddress3 ) );
        assertFalse( shoppingBasketUseCases.isPaymentAuthorized( mailAddress3 ) );
        orderHistory = shoppingBasketUseCases.getOrderHistory( mailAddress3 );
        assertEquals( 2, orderHistory.size() );
        assertEquals( 4, orderHistory.get( itemId100EUR ).intValue() );
        assertEquals( 4, orderHistory.get( itemId25EUR ).intValue() );
    }


    @Test
    public void testShoppingBasketLogisticsLimits() {
        // given
        UUID itemId100EUR = (UUID) TestHelper.ITEM_DATA[13][0];
        UUID itemId25EUR = (UUID) TestHelper.ITEM_DATA[12][0];
        UUID itemId1EUR = (UUID) TestHelper.ITEM_DATA[4][0];
        String mailAddress7 = TestHelper.USER_DATA[7][1];
        assertTrue( shoppingBasketUseCases.isEmpty( mailAddress7 ) );
        assertFalse( shoppingBasketUseCases.isPaymentAuthorized( mailAddress7 ) );

        // when
        shoppingBasketUseCases.addItemToShoppingBasket( mailAddress7, itemId100EUR, 4 );
        shoppingBasketUseCases.addItemToShoppingBasket( mailAddress7, itemId25EUR, 3 );
        shoppingBasketUseCases.addItemToShoppingBasket( mailAddress7, itemId1EUR, 14 );
        assertFalse( shoppingBasketUseCases.isEmpty( mailAddress7 ) );
        assertEquals( 489.0f, shoppingBasketUseCases.getShoppingBasketAsMoneyValue( mailAddress7 ), 0.01f );
        assertFalse( shoppingBasketUseCases.isPaymentAuthorized( mailAddress7 ) );

        // then
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.checkout( mailAddress7 ) );
        assertTrue( shoppingBasketUseCases.isPaymentAuthorized( mailAddress7 ) );
        assertFalse( shoppingBasketUseCases.isEmpty( mailAddress7 ) );
        Map<UUID, Integer> orderHistory = shoppingBasketUseCases.getOrderHistory( mailAddress7 );
        assertEquals( 0, orderHistory.size() );

        // and then remove 1 item and it should be ok (<= 20)
        shoppingBasketUseCases.removeItemFromShoppingBasket( mailAddress7, itemId1EUR, 1 );
        assertEquals( 488.0f, shoppingBasketUseCases.getShoppingBasketAsMoneyValue( mailAddress7 ), 0.01f );
        assertDoesNotThrow( () -> shoppingBasketUseCases.checkout( mailAddress7 ) );
        assertTrue( shoppingBasketUseCases.isEmpty( mailAddress7 ) );
        assertFalse( shoppingBasketUseCases.isPaymentAuthorized( mailAddress7 ) );
        orderHistory = shoppingBasketUseCases.getOrderHistory( mailAddress7 );
        assertEquals( 3, orderHistory.size() );
        assertEquals( 4, orderHistory.get( itemId100EUR ).intValue() );
        assertEquals( 3, orderHistory.get( itemId25EUR ).intValue() );
        assertEquals( 13, orderHistory.get( itemId1EUR ).intValue() );
    }


    @Test
    public void testNoCheckoutForEmptyShoppingBasket() {
        // given
        String mailAddress3 = TestHelper.USER_DATA[3][1];
        assertTrue( shoppingBasketUseCases.isEmpty( mailAddress3 ) );
        assertFalse( shoppingBasketUseCases.isPaymentAuthorized( mailAddress3 ) );

        // when
        // then
        assertThrows( ShopException.class, () -> shoppingBasketUseCases.checkout( mailAddress3 ) );
        assertFalse( shoppingBasketUseCases.isPaymentAuthorized( mailAddress3 ) );
        assertTrue( shoppingBasketUseCases.isEmpty( mailAddress3 ) );
        Map<UUID, Integer> orderHistory = shoppingBasketUseCases.getOrderHistory( mailAddress3 );
        assertEquals( 0, orderHistory.size() );
    }
}
