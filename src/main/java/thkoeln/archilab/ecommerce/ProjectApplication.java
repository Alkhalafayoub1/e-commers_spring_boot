package thkoeln.archilab.ecommerce;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import thkoeln.archilab.ecommerce.solution.inventory.domain.Inventory;
import thkoeln.archilab.ecommerce.solution.inventory.domain.InventoryRepository;


@SpringBootApplication
public class ProjectApplication implements CommandLineRunner {

    @Autowired
    InventoryRepository inventoryRepository;

    /**
     * Entry method
     *
     * @param args
     */

    public static void main(String[] args) {

        SpringApplication.run(ProjectApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        inventoryRepository.save(new Inventory());
    }


}