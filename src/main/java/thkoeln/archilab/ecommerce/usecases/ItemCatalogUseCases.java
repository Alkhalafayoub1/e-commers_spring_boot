package thkoeln.archilab.ecommerce.usecases;

import java.util.UUID;


/**
 * This interface contains methods needed in the context of use cases dealing with managing the item catalog.
 * The interface is probably incomplete, and will grow over time.
 */

public interface ItemCatalogUseCases {
    /**
     * Adds a new item to the shop catalog
     *
     * @param itemId
     * @param name
     * @param description
     * @param size
     * @param purchasePrice
     * @param salesPrice
     * @throws ShopException if ...
     *                       - the item id already exists,
     *                       - name or description are null or empty,
     *                       - the size is <= 0 (but can be null!),
     *                       - the purchase price is null or <= 0,
     *                       - the sales price is null or <= 0,
     *                       - the sales price is lower than the purchase price
     */
    public void addItemToCatalog(UUID itemId, String name, String description, Float size,
                                 Float purchasePrice, Float salesPrice);


    /**
     * Removes a item from the shop catalog
     *
     * @param itemId
     * @throws ShopException if
     *                       - the item id does not exist
     *                       - the item is still in inventory
     *                       - the item is still reserved in a shopping basket, or part of a completed order
     */
    public void removeItemFromCatalog(UUID itemId);


    /**
     * Get the sales price of a given item
     *
     * @param itemId
     * @return the sales price
     * @throws ShopException if the item id does not exist
     */
    public Float getSalesPrice(UUID itemId);


    /**
     * Clears the item catalog, i.e. removes all items from the catalog, including all the inventory,
     * all the reservations and all the orders.
     */
    public void deleteItemCatalog();


}
