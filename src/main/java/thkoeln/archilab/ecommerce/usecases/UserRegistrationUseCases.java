package thkoeln.archilab.ecommerce.usecases;

/**
 * This interface contains methods needed in the context of use cases dealing with registering a user.
 * The interface is probably incomplete, and will grow over time.
 */
public interface UserRegistrationUseCases {
    /**
     * Registers a new user
     *
     * @param name
     * @param mailAddress
     * @param street
     * @param city
     * @param postalCode
     * @throws ShopException if ...
     *                       - user with the given mail address already exists
     *                       - if the data are invalid (name, mail address, street, city, postal code empty or null)
     */
    public void register(String name, String mailAddress, String street, String city, String postalCode);


    /**
     * Changes the address of a user
     *
     * @param userMailAddress
     * @param street
     * @param city
     * @param postalCode
     * @throws ShopException if ...
     *                       - the user with the given mail address does not exist,
     *                       - the address data are invalid (street, city, postal code empty or null)
     */
    public void changeAddress(String userMailAddress, String street, String city, String postalCode);


    /**
     * Returns the data of a user as an array of strings (name, mailAddress, street, city, postalCode)
     *
     * @param userMailAddress
     * @return the user data
     * @throws ShopException if the user with the given mail address does not exist
     */
    public String[] getUserData(String userMailAddress);


    /**
     * Deletes all users, including all orders and shopping baskets
     */
    public void deleteAllUsers();


}
