package thkoeln.archilab.ecommerce.usecases;

import java.util.Map;
import java.util.UUID;

/**
 * This interface contains methods needed in the context of use cases dealing with logistics,
 * i.e. the delivery of items to a user. It is probably incomplete, and will grow over time.
 */
public interface DeliveryUseCases {
    /**
     * Delivers a item to a user. The item is identified by its id, and the user by
     * his/her name, street, city and postal code.
     *
     * @param deliveryRecipient
     * @param deliveryContent   - a map of item ids and quantities
     * @return the id of the delivery, if successfully triggered
     * @throws ShopException if ...
     *                       - deliveryRecipient is null
     *                       - any of the properties in deliveryRecipient (the getXxx(...) methods) return null or empty strings
     *                       - deliveryContent is null or empty
     *                       - the total number of items in the delivery is > 20
     */
    public UUID triggerDelivery(DeliveryRecipient deliveryRecipient, Map<UUID, Integer> deliveryContent);


    /**
     * Returns a map showing which items have been delivered to a user, and how many of each item
     *
     * @param userMailAddress
     * @return the delivery history of the user (map is empty if the user has not had any deliveries yet)
     * @throws ShopException if
     *                       - the mail address is null or empty
     *                       - the user with the given mail address does not exist
     */
    public Map<UUID, Integer> getDeliveryHistory(String userMailAddress);


    /**
     * Deletes all delivery history.
     */
    public void deleteDeliveryHistory();
}
