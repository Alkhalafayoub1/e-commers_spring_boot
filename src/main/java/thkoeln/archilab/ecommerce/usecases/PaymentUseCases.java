package thkoeln.archilab.ecommerce.usecases;

import java.util.UUID;

/**
 * This interface contains methods needed in the context of use cases dealing with payments.
 * The interface is probably incomplete, and will grow over time. For the moment, we don't
 * have payment details for a user (that will come later). For now, we identify a user
 * just by his/her mail address.
 */
public interface PaymentUseCases {

    /**
     * Authorizes a payment for a user (identified by his/her mail address) for a given amount
     *
     * @param userMailAddress
     * @param amount
     * @return the id of the payment, if successfully authorized
     * @throws ShopException if ...
     *                       - userMailAddress is null or empty
     *                       - the user with the given mail address does not exist
     *                       - the amount is <= 0.00 EUR
     *                       - the payment cannot be processed, because it is over the limit of 500.00 EUR
     */
    public UUID authorizePayment(String userMailAddress, Float amount);


    /**
     * Returns the total amount of payments (over the complete history) for a user
     * (identified by his/her mail address)
     *
     * @param userMailAddress
     * @return the total amount of payments for the user with the given mailAddress,
     * or 0.00 EUR if the user has not made any payments yet.
     * @throws ShopException if ...
     *                       - userMailAddress is null or empty
     *                       - the user with the given mailAddress does not exist
     */
    public Float getPaymentTotal(String userMailAddress);


    /**
     * Deletes all payment history, for all users.
     */
    public void deletePaymentHistory();
}
