package thkoeln.archilab.ecommerce.usecases;

import java.util.Map;
import java.util.UUID;

/**
 * This interface contains methods needed in the context of use cases dealing with the shopping basket.
 * The interface is probably incomplete, and will grow over time.
 */
public interface ShoppingBasketUseCases {
    /**
     * Adds a item to the shopping basket of a user
     *
     * @param userMailAddress
     * @param itemId
     * @param quantity
     * @throws ShopException if ...
     *                       - the user with the given mailAddress does not exist,
     *                       - the item does not exist,
     *                       - the quantity is negative,
     *                       - the item is not available in the requested quantity
     */
    public void addItemToShoppingBasket(String userMailAddress, UUID itemId, int quantity);


    /**
     * Removes a item from the shopping basket of a user
     *
     * @param userMailAddress
     * @param itemId
     * @param quantity
     * @throws ShopException if ...
     *                       - the user with the given mailAddress does not exist,
     *                       - the item does not exist
     *                       - the quantity is negative
     *                       - the item is not in the shopping basket in the requested quantity
     */
    public void removeItemFromShoppingBasket(String userMailAddress, UUID itemId, int quantity);


    /**
     * Returns a map showing which items are in the shopping basket of a user and how many of each item
     *
     * @param userMailAddress
     * @return the shopping basket of the user (map is empty if the shopping basket is empty)
     * @throws ShopException if the user with the given mailAddress does not exist
     */
    public Map<UUID, Integer> getShoppingBasketAsMap(String userMailAddress);


    /**
     * Returns the current value of all items in the shopping basket of a user
     *
     * @param userMailAddress
     * @return the shopping basket of the user
     * @throws ShopException if the user with the given mail address does not exist
     */
    public float getShoppingBasketAsMoneyValue(String userMailAddress);


    /**
     * Get the number units of a specific item that are currently reserved in the shopping baskets of all users
     *
     * @param itemId
     * @throws ShopException if the item id does not exist
     */
    public int getReservedInventoryInShoppingBaskets(UUID itemId);


    /**
     * Checks if the shopping basket of a user is empty
     *
     * @param userMailAddress
     * @return true if the shopping basket is empty, false otherwise
     * @throws ShopException if ...
     *                       - the mail address is null or empty
     *                       - the user with the given mail address does not exist
     */
    public boolean isEmpty(String userMailAddress);


    /**
     * Checks if the payment for a specific shopping basket of a user has been authorized to be paid,
     * i.e. the shopping basket is not empty, the user has given his/her payment details, and the payment
     * has been authorized (under the limits of the user's credit card). However, the order
     * has not yet been placed yet, and the logistics details (delivery address) have not yet been given.
     *
     * @param userMailAddress
     * @return true if the payment has been authorized, false otherwise
     * @throws ShopException if ...
     *                       - the mail address is null or empty
     *                       - the user with the given mail address does not exist
     */
    public boolean isPaymentAuthorized(String userMailAddress);


    /**
     * Checks out the shopping basket of a user
     *
     * @param userMailAddress
     * @throws ShopException if the user with the given mail address does not exist, or if the shopping basket is empty
     */
    public void checkout(String userMailAddress);


    /**
     * Returns a map showing which items have been ordered by a user, and how many of each item
     *
     * @param userMailAddress
     * @return the order history of the user (map is empty if the user has not ordered anything yet)
     * @throws ShopException if
     *                       - the mail address is null or empty
     *                       - the user with the given mail address does not exist
     * @Deprecated Might be split into a dedicated OrderUseCases interface later (but still valid in this milestone)
     */
    public Map<UUID, Integer> getOrderHistory(String userMailAddress);


    /**
     * Deletes all orders and shopping baskets in the system
     *
     * @Deprecated Might be split into two methods later (delete orders and delete shopping baskets), with
     * the order deletion moved to a dedicated OrderUseCases interface later
     * (but still valid in this milestone)
     */
    public void deleteAllOrders();

}
