package thkoeln.archilab.ecommerce.solution.Delivery.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface DeliveryRepository extends JpaRepository<Delivery, UUID> {
    List<Delivery> findAllByUserMailAddress(String mail);
}
