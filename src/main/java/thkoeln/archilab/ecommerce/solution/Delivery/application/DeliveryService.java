package thkoeln.archilab.ecommerce.solution.Delivery.application;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import thkoeln.archilab.ecommerce.solution.Delivery.domain.Delivery;
import thkoeln.archilab.ecommerce.solution.Delivery.domain.DeliveryRepository;
import thkoeln.archilab.ecommerce.solution.item.domain.ItemRepository;
import thkoeln.archilab.ecommerce.usecases.DeliveryRecipient;
import thkoeln.archilab.ecommerce.usecases.DeliveryUseCases;
import thkoeln.archilab.ecommerce.usecases.ShopException;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@AllArgsConstructor
public class DeliveryService implements DeliveryUseCases {
    private final DeliveryRepository deliveryRepository;
    private final ItemRepository itemRepository;

    @Override
    public UUID triggerDelivery(DeliveryRecipient deliveryRecipient, Map<UUID, Integer> deliveryContent) {
        if (deliveryRecipient == null) {
            throw new ShopException("deliveryDelivery is null ");
        }
        if (deliveryRecipient.getMailAddress() == null || deliveryRecipient.getMailAddress().equals("")) {
            throw new ShopException("deliveryContent is null or empty");
        }
        if (deliveryRecipient.getName() == null || deliveryRecipient.getName().equals("")) {
            throw new ShopException("select a city");
        }
        if (deliveryRecipient.getCity() == null || deliveryRecipient.getCity().equals("")) {
            throw new ShopException("select a city");
        }
        if (deliveryRecipient.getStreet() == null || deliveryRecipient.getStreet().equals("")) {
            throw new ShopException("select a street");
        }
        if (deliveryRecipient.getPostalCode() == null || deliveryRecipient.getPostalCode().equals("")) {
            throw new ShopException("select a post code");
        }
        //Optional<User> theUser = userRepository.findByMailAddress(deliveryRecipient.getMailAddress());
        //if (theUser.isEmpty()) {
        //    throw new ShopException("the user with the given mailAddress does not exist");
        //}
        if (deliveryContent == null || deliveryContent.isEmpty()) {
            throw new ShopException("the user with the given mailAddress does not exist");
        }
        AtomicInteger count= new AtomicInteger(0);
        deliveryContent.forEach((item,q)->{
            count.addAndGet(q);
        });

        if (count.get() > 20) {
            throw new ShopException("the total number of items in the delivery is > 20");
        }


        Delivery delivery = new Delivery();
        delivery.setId(UUID.randomUUID());
        delivery.setName(deliveryRecipient.getName());
        delivery.setUserMailAddress(deliveryRecipient.getMailAddress());
        delivery.setStreet(deliveryRecipient.getStreet());
        delivery.setCity(deliveryRecipient.getCity());
        delivery.setPostalCode(deliveryRecipient.getPostalCode());
delivery.setDeliveryContent(deliveryContent);
        deliveryRepository.save(delivery);
        return delivery.getId();
    }

    @Override
    public Map<UUID, Integer> getDeliveryHistory(String userMailAddress) {
        List<Delivery> deliveries = deliveryRepository.findAllByUserMailAddress(userMailAddress);
        Map<UUID, Integer> history = new HashMap<>();
       deliveries.forEach(delivery -> {
           delivery.getDeliveryContent().forEach((item,quantity)->{
               history.merge(item,quantity,Integer::sum);
               if(history.get(item)==null) history.put(item,quantity);
           });
       });

        return history;
    }

    @Override
    public void deleteDeliveryHistory() {
        deliveryRepository.deleteAll();
    }
}
