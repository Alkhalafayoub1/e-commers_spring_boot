package thkoeln.archilab.ecommerce.solution.Delivery.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import thkoeln.archilab.ecommerce.usecases.DeliveryRecipient;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Delivery implements DeliveryRecipient {
    @ElementCollection
    Map<UUID, Integer> deliveryContent = new HashMap<>();
    @Id
    private UUID id;
    private String userMailAddress;
    private String name;
    private String street;
    private String city;
    private String postalCode;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getMailAddress() {
        return this.userMailAddress;
    }

    @Override
    public String getStreet() {
        return this.street;
    }

    @Override
    public String getCity() {
        return this.city;
    }

    @Override
    public String getPostalCode() {
        return this.postalCode;
    }
}
