package thkoeln.archilab.ecommerce.solution.user.domain;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import thkoeln.archilab.ecommerce.usecases.DeliveryRecipient;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class User implements DeliveryRecipient {
    @Id
    private UUID id;
    private String name;
    @Column(name = "mailAddress", unique = true)
    private String mailAddress;
    @Embedded
    private Address address;


    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getMailAddress() {
        return mailAddress;
    }

    @Override
    public String getStreet() {
        return address.getStreet();
    }

    @Override
    public String getCity() {
        return address.getCity();
    }

    @Override
    public String getPostalCode() {
        return address.getPostalCode();
    }
}
