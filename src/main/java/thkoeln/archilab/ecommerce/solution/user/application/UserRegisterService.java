package thkoeln.archilab.ecommerce.solution.user.application;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import thkoeln.archilab.ecommerce.solution.user.domain.Address;
import thkoeln.archilab.ecommerce.solution.user.domain.User;
import thkoeln.archilab.ecommerce.solution.user.domain.UserRepository;
import thkoeln.archilab.ecommerce.usecases.ShopException;
import thkoeln.archilab.ecommerce.usecases.UserRegistrationUseCases;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserRegisterService implements UserRegistrationUseCases {
    private final UserRepository userRepository;



    @Override
    public void register(String name, String mailAddress, String street, String city, String postalCode) {
        if (mailAddress == null || mailAddress.isEmpty()) {
            throw new ShopException("select a emailaddress");
        }
        Optional<User> theUser = userRepository.findByMailAddress(mailAddress);

        if (city == null || city.equals("")) {
            throw new ShopException("select a city");
        }
        if (name == null || name.equals("")) {
            throw new ShopException("select a city");
        }
        if (street == null || street.equals("")) {
            throw new ShopException("select a street");
        }
        if (postalCode == null || postalCode.equals("")) {
            throw new ShopException("select a post code");
        }
        if (theUser.isPresent()) {
            throw new ShopException("the user with the given mailAddress is already exist");
        }

        Address address = new Address();
        address.setCity(city);
        address.setStreet(street);
        address.setPostalCode(postalCode);
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setName(name);
        user.setMailAddress(mailAddress);
        user.setAddress(address);
        userRepository.save(user);


    }


    @Override
    public void changeAddress(String userMailAddress, String street, String city, String postalCode) {
        Optional<User> theUser = userRepository.findByMailAddress(userMailAddress);
        if (theUser.isEmpty()) {
            throw new ShopException("A user with " + userMailAddress + " is not existed");
        }
        if (city == null || city.equals("")) {
            throw new ShopException("select a city");
        }
        if (street == null || street.equals("")) {
            throw new ShopException("select a street");
        }
        if (postalCode == null || postalCode.equals("")) {
            throw new ShopException("select a post code");
        }
        Address address = new Address();
        address.setCity(city);
        address.setStreet(street);
        address.setPostalCode(postalCode);
        theUser.get().setAddress(address);
        userRepository.save(theUser.get());


    }

    @Override
    public String[] getUserData(String userMailAddress) {
        String[] user = new String[5];
        Optional<User> theUser = userRepository.findByMailAddress(userMailAddress);
        if (theUser.isEmpty()) {
            throw new ShopException("A user with " + userMailAddress + " is not existed");
        }
        System.out.println(theUser.get());
        user[0] = theUser.get().getName();
        user[1] = theUser.get().getMailAddress();
        user[2] = theUser.get().getStreet();
        user[3] = theUser.get().getCity();
        user[4] = theUser.get().getPostalCode();
        return user;
    }

    @Override
    public void deleteAllUsers() {

        userRepository.deleteAll();
    }


}
