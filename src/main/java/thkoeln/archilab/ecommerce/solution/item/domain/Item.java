package thkoeln.archilab.ecommerce.solution.item.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Item {
    @Id
    private UUID id;
    private String name;
    private String description;
    private Float size;
    private Float purchasePrice;
    private Float salesPrice;
}
