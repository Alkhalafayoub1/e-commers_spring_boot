package thkoeln.archilab.ecommerce.solution.item.application;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import thkoeln.archilab.ecommerce.solution.item.domain.Item;
import thkoeln.archilab.ecommerce.solution.item.domain.ItemRepository;
import thkoeln.archilab.ecommerce.solution.user.domain.User;
import thkoeln.archilab.ecommerce.solution.user.domain.UserRepository;
import thkoeln.archilab.ecommerce.usecases.InventoryManagementUseCases;
import thkoeln.archilab.ecommerce.usecases.ItemCatalogUseCases;
import thkoeln.archilab.ecommerce.usecases.ShopException;
import thkoeln.archilab.ecommerce.usecases.ShoppingBasketUseCases;

import java.util.*;

@Service
public class ItemService implements ItemCatalogUseCases {
    private final ItemRepository itemRepository;
    private final InventoryManagementUseCases inventoryManagementUseCases;
  //  private final OrderRepository orderRepository;
    private final ShoppingBasketUseCases shoppingBasketUseCases;
    private final UserRepository userRepository;

    public ItemService(ItemRepository itemRepository,
                      @Lazy InventoryManagementUseCases inventoryManagementUseCases,
                       ShoppingBasketUseCases shoppingBasketUseCases,
                       UserRepository userRepository) {
        this.itemRepository = itemRepository;
        this.inventoryManagementUseCases = inventoryManagementUseCases;
        this.shoppingBasketUseCases = shoppingBasketUseCases;
        this.userRepository = userRepository;
    }

    @Override
    public void addItemToCatalog(UUID itemId, String name, String description, Float size, Float purchasePrice, Float salesPrice) {

        if (itemId == null) {
            throw new ShopException("the id is null");
        }
        Optional<Item> item = itemRepository.findById(itemId);
        if (item.isPresent()) {
            throw new ShopException("the item by id " + item + " already exists");
        }
        if (name == null || name.equals("")) {
            throw new ShopException("the item Name is null");
        }
        if (description == null || description.equals("")) {
            throw new ShopException("the item description is null");
        }
        if (size != null && size <= 0) {
            throw new ShopException("the item size lower than 0");
        }
        if (purchasePrice == null || purchasePrice <= 0f) {
            throw new ShopException("the purchase price is null or negative");
        }
        if (salesPrice == null || salesPrice <= 0) {
            throw new ShopException("the sales price is null or negative");
        }
        if (salesPrice < purchasePrice) {
            throw new ShopException("the sales price is lower than the purchase price");
        }
        if (itemId == null) {
            throw new ShopException("the id is ");
        }
        Item item1 = new Item();
        item1.setId(itemId);
        item1.setName(name);
        item1.setDescription(description);
        item1.setSize(size);
        item1.setPurchasePrice(purchasePrice);
        item1.setSalesPrice(salesPrice);
        itemRepository.save(item1);


    }

    @Override
    public void removeItemFromCatalog(UUID itemId) {
        if (itemId == null || itemId.equals("")) {
            throw new ShopException("select a emailaddress");
        }
        Optional<Item> item = itemRepository.findById(itemId);
        if (item.isEmpty()) {
            throw new ShopException("the item by id " + item + " ist not exists");
        }
        List<User> users = userRepository.findAll();
        users.forEach(user -> {
            shoppingBasketUseCases.getOrderHistory(user.getMailAddress()).forEach((id,q)->{
                if (id.equals(itemId))throw new ShopException("the item is in order");
            });


        });
        int itemInInventory= inventoryManagementUseCases.getAvailableInventory(itemId);

        if (itemInInventory>0) {
            throw new ShopException("the item is in inInventory");
        }
        itemRepository.deleteById(itemId);

    }
    @Override
    public Float getSalesPrice(UUID itemId) {
        if (itemId == null || itemId.equals("")) {
            throw new ShopException("select a emailaddress");
        }
        Optional<Item> item = itemRepository.findById(itemId);
        if (item.isEmpty()) {
            throw new ShopException("the item by id " + item + " is not exists");
        }
        if (item.get().getId() != itemId) {
            throw new ShopException("the item by id is not equal the given Id");
        }
        return item.get().getSalesPrice();
    }

    @Override
    public void deleteItemCatalog() {
        itemRepository.deleteAll();

    }


}

