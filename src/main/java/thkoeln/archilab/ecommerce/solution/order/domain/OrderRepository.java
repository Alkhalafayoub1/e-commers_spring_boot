package thkoeln.archilab.ecommerce.solution.order.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, UUID> {
      List<Order> findAllByUserMailAddress(String email);

}
