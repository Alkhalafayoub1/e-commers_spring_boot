package thkoeln.archilab.ecommerce.solution.order.domain;

public enum OrderStatus {
    CREATED,
    DELETED
}
