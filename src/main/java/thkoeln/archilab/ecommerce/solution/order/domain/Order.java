package thkoeln.archilab.ecommerce.solution.order.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import thkoeln.archilab.ecommerce.solution.item.domain.Item;
import thkoeln.archilab.ecommerce.solution.user.domain.User;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Entity(name = "OrderEntity")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    //@ElementCollection
    //private List<ItemLine> itemLines=new ArrayList<>();
    @ElementCollection
    Map<Item, Integer> items = new HashMap<>();
    @Id
    private UUID id;
    @ManyToOne (cascade = CascadeType.ALL)
    private User user;
    private OrderStatus orderStatus;
}
