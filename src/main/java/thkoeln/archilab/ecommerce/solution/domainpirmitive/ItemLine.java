package thkoeln.archilab.ecommerce.solution.domainpirmitive;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import thkoeln.archilab.ecommerce.solution.item.domain.Item;

import javax.persistence.Embeddable;
import javax.persistence.OneToOne;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemLine {
    @OneToOne
    private Item item;
    private Integer quantity;
    //@ElementCollection
    //private HashMap<UUID,Integer>  items=new HashMap<>();
}
