package thkoeln.archilab.ecommerce.solution.payment.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Payment {
    @Id
    private UUID id;
    private String userMailAddress;
    private float amount;


}
