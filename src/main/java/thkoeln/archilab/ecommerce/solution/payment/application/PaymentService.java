package thkoeln.archilab.ecommerce.solution.payment.application;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import thkoeln.archilab.ecommerce.solution.payment.domain.Payment;
import thkoeln.archilab.ecommerce.solution.payment.domain.PaymentRepository;
import thkoeln.archilab.ecommerce.solution.user.domain.User;
import thkoeln.archilab.ecommerce.solution.user.domain.UserRepository;
import thkoeln.archilab.ecommerce.usecases.PaymentUseCases;
import thkoeln.archilab.ecommerce.usecases.ShopException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class PaymentService implements PaymentUseCases {
    private final UserRepository userRepository;

    private final PaymentRepository paymentRepository;

    @Override
    public UUID authorizePayment(String userMailAddress, Float amount) {

        if (userMailAddress == null || userMailAddress.equals("")) {
            throw new ShopException("select a emailaddress");
        }

        Optional<User> theUser = userRepository.findByMailAddress(userMailAddress);
        if (theUser.isEmpty()){
            throw new ShopException("the user with the given mailAddress does not exist");
        }

        if (amount == null) {
            throw new ShopException("the amount is null");
        }
        if (amount <= 0.00f) {
            throw new ShopException("the amount is <= 0.00 EUR");
        }
        if (amount > 500.00) {
            throw new ShopException("the amount is over the limit of 500.00 EUR");
        }
        Payment payment = new Payment();
        payment.setId(UUID.randomUUID());
        payment.setUserMailAddress(userMailAddress);
        payment.setAmount(amount);
        paymentRepository.save(payment);
        return payment.getId();
    }

    @Override
    public Float getPaymentTotal(String userMailAddress) {
        if (userMailAddress == null || userMailAddress.equals("")) {
            throw new ShopException("select a emailaddress");
        }

        Optional<User> theUser = userRepository.findByMailAddress(userMailAddress);
        if (theUser.isEmpty()){
            throw new ShopException("the user with the given mailAddress does not exist");
        }
        List<Payment> paymentList = paymentRepository.findAllByUserMailAddress(userMailAddress);
        List<Float> amounts = paymentList.stream().map(Payment::getAmount).collect(Collectors.toList());
        float totalAmount = 0.00f;
        //amounts.forEach(amount -> totalAmount=totalAmount+amount);
        for (float amount : amounts) {
            totalAmount = totalAmount + amount;
        }
        if (totalAmount == 0.00f) {
            return 0.00f;
        }

        return totalAmount;
    }

    @Override
    public void deletePaymentHistory() {
        paymentRepository.deleteAll();

    }
}
