package thkoeln.archilab.ecommerce.solution.payment.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PaymentRepository extends JpaRepository<Payment, UUID> {

    Optional<Payment> findPaymentByUserMailAddress(String email);

    List<Payment> findAllByUserMailAddress(String mail);
}
