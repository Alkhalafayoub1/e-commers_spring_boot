package thkoeln.archilab.ecommerce.solution.inventory.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import thkoeln.archilab.ecommerce.solution.item.domain.Item;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Inventory {
    @Id
    private UUID id = UUID.randomUUID();
    //@ElementCollection
    //private List<ItemLine> itemLines=new ArrayList<>();
    @ElementCollection
    private Map<Item, Integer> items = new HashMap<>();
}
