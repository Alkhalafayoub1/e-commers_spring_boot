package thkoeln.archilab.ecommerce.solution.inventory.application;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import thkoeln.archilab.ecommerce.solution.inventory.domain.Inventory;
import thkoeln.archilab.ecommerce.solution.inventory.domain.InventoryRepository;
import thkoeln.archilab.ecommerce.solution.item.domain.Item;
import thkoeln.archilab.ecommerce.solution.item.domain.ItemRepository;
import thkoeln.archilab.ecommerce.solution.order.domain.OrderRepository;
import thkoeln.archilab.ecommerce.solution.shoppingbasket.domain.ShoppingBasket;
import thkoeln.archilab.ecommerce.solution.shoppingbasket.domain.ShoppingBasketRepository;
import thkoeln.archilab.ecommerce.usecases.InventoryManagementUseCases;
import thkoeln.archilab.ecommerce.usecases.ItemCatalogUseCases;
import thkoeln.archilab.ecommerce.usecases.ShopException;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@AllArgsConstructor
public class InventoryService implements InventoryManagementUseCases {
    private final ItemCatalogUseCases itemCatalogUseCases;
    private final InventoryRepository inventoryRepository;
    //   private final ShoppingBasketUseCases shoppingBasketUseCases;
    private final ItemRepository itemRepository;
    private ShoppingBasketRepository shoppingBasketRepository;
    private OrderRepository orderRepository;

    @Override
    public void addToInventory(UUID itemId, int addedQuantity) {
        if (itemId == null || itemId.equals("")) {
            throw new ShopException("select a emailaddress");
        }
        if (addedQuantity < 0) {
            throw new ShopException("the quantity is negative");
        }

        Optional<Item> item = itemRepository.findById(itemId);
        if (item.isEmpty()) {
            throw new ShopException("the item by id " + item + " is not exists");
        }
        Inventory inventory = inventoryRepository.findAll().get(0);
        inventory.getItems().forEach((k, v) -> {
            if (k.getId().equals(itemId)) {
                inventory.getItems().merge(k, addedQuantity,Integer::sum);
            }
        });
        if(inventory.getItems().get(item.get())==null)inventory.getItems().put(item.get(), addedQuantity);
        inventoryRepository.save(inventory);
    }

    @Override
    public void removeFromInventory(UUID itemId, int removedQuantity) {
        if (itemId == null || itemId.equals("")) {
            throw new ShopException("select a emailaddress");
        }
        if (removedQuantity < 0||removedQuantity>getAvailableInventory(itemId)) {
            throw new ShopException("the quantity is negative");
        }
         float itemPrice=itemCatalogUseCases.getSalesPrice(itemId);
         if (itemPrice<=0){
             throw new ShopException("the item is not exist");
         }

        Optional<Item> item = itemRepository.findById(itemId);
        if (item.isEmpty()) {
            throw new ShopException("the item by id " + item + " is not exists");
        }
        Inventory inventory = inventoryRepository.findAll().get(0);
        if (inventory.getItems().isEmpty()) {
            throw new ShopException("the inventory is empty");
        }






        AtomicInteger totalRemoved = new AtomicInteger(0);
        inventory.getItems().forEach((k, v) -> {
            if (k.getId().equals(itemId)) {
                if (v >= removedQuantity) {
                    totalRemoved.set(removedQuantity);
                    inventory.getItems().put(k, v - removedQuantity);

                } else {
                    totalRemoved.addAndGet(v);
                    inventory.getItems().put(k, 0);
                }

            }
        });
        if (totalRemoved.get() < removedQuantity) {
            List<ShoppingBasket> shoppingBaskets = shoppingBasketRepository.findAll();
for (ShoppingBasket shoppingBasket:shoppingBaskets){
    shoppingBasket.getItems().forEach((it,quan)->{
        if(it.getId().equals(itemId)){
            if(removedQuantity-totalRemoved.get()<=quan){
                shoppingBasket.getItems().put(it,quan-(removedQuantity-totalRemoved.get()));
                totalRemoved.set(removedQuantity);
            }else {
                shoppingBasket.getItems().put(it,0);
                totalRemoved.addAndGet(quan);

            }
        }
    });
    if (shoppingBasket.getItems().get(item.get())==0)shoppingBasket.getItems().remove(item.get());
    shoppingBasketRepository.save(shoppingBasket);
}




            }


        inventoryRepository.save(inventory);

    }

    @Override
    public void changeInventoryTo(UUID itemId, int newTotalQuantity) {
        if (itemId == null || itemId.equals("")) {
            throw new ShopException("select a emailaddress");
        }
        if (newTotalQuantity < 0) {
            throw new ShopException("the quantity is negative");
        }
        Optional<Item> item = itemRepository.findById(itemId);
        if (item.isEmpty()) {
            throw new ShopException("the item by id " + item + " is not exists");
        }

        Inventory inventory = inventoryRepository.findAll().get(0);

        if (newTotalQuantity > getAvailableInventory(itemId)) {
            System.out.println(Arrays.toString(inventory.getItems().keySet().toArray()));
            inventory.getItems().keySet().forEach((k) -> {
                if (k.getId().equals(itemId)) {
                    inventory.getItems().put(k,  inventory.getItems().get(k) + (newTotalQuantity - getAvailableInventory(itemId)));
                }
            });
            inventoryRepository.save(inventory);
        } else removeFromInventory(itemId, getAvailableInventory(itemId) - newTotalQuantity);

    }

    @Override
    public int getAvailableInventory(UUID itemId) {
        if (itemId == null || itemId.equals("")) {
            throw new ShopException("select a emailaddress");
        }
        //float itemPrice=itemCatalogUseCases.getSalesPrice(itemId);
        //if (itemPrice<=0){
        //    throw new ShopException("the item is not exist");
        //}
        Optional<Item> item = itemRepository.findById(itemId);
        if (item.isEmpty()) {
            throw new ShopException("the item by id " + item + " is not exists");
        }
        Inventory inventory = inventoryRepository.findAll().get(0);

        List<ShoppingBasket> shoppingBaskets = shoppingBasketRepository.findAll();
        AtomicInteger quantity = new AtomicInteger(0);
       shoppingBaskets.forEach(b->{
           b.getItems().forEach((k, v) -> {
               if (k.getId().equals(itemId)) {
                   quantity.addAndGet(quantity.get() + v);

               }
           });
       });


            inventory.getItems().forEach((k, v) -> {
                if (k.getId().equals(itemId)) {
                    quantity.addAndGet(v);
                }
            });


        return quantity.get();
    }


}