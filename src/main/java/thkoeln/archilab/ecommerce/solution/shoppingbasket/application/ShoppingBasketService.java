package thkoeln.archilab.ecommerce.solution.shoppingbasket.application;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import thkoeln.archilab.ecommerce.solution.Delivery.domain.Delivery;
import thkoeln.archilab.ecommerce.solution.item.domain.Item;
import thkoeln.archilab.ecommerce.solution.item.domain.ItemRepository;
import thkoeln.archilab.ecommerce.solution.order.domain.Order;
import thkoeln.archilab.ecommerce.solution.order.domain.OrderRepository;
import thkoeln.archilab.ecommerce.solution.order.domain.OrderStatus;
import thkoeln.archilab.ecommerce.solution.shoppingbasket.domain.ShoppingBasket;
import thkoeln.archilab.ecommerce.solution.shoppingbasket.domain.ShoppingBasketRepository;
import thkoeln.archilab.ecommerce.solution.shoppingbasket.domain.ShoppingBasketStatus;
import thkoeln.archilab.ecommerce.solution.user.domain.User;
import thkoeln.archilab.ecommerce.solution.user.domain.UserRepository;
import thkoeln.archilab.ecommerce.usecases.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class ShoppingBasketService implements ShoppingBasketUseCases {
    //private final UserRegistrationUseCases userRegistrationUseCases;
    private final ItemRepository itemRepository;
    private final InventoryManagementUseCases inventoryManagementUseCases;
    private final PaymentUseCases paymentUseCases;
    private final DeliveryUseCases deliveryUseCases;
    private final ShoppingBasketRepository shoppingBasketRepository;
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;

    public ShoppingBasketService(ItemRepository itemRepository,
                                @Lazy InventoryManagementUseCases inventoryManagementUseCases,
                                 PaymentUseCases paymentUseCases,
                                 ShoppingBasketRepository shoppingBasketRepository,
                                 OrderRepository orderRepository,
                                 UserRepository userRepository,
                                 DeliveryUseCases deliveryUseCases ) {
        this.itemRepository = itemRepository;
        this.inventoryManagementUseCases = inventoryManagementUseCases;
        this.paymentUseCases = paymentUseCases;
        this.shoppingBasketRepository = shoppingBasketRepository;
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.deliveryUseCases = deliveryUseCases;
    }

    @Override
    public void addItemToShoppingBasket(String userMailAddress, UUID itemId, int quantity) {
        if (userMailAddress == null || userMailAddress.equals("")) {
            throw new ShopException("select a emailaddress");
        }
        Optional<Item> item = itemRepository.findById(itemId);
        if (!item.isPresent()||quantity < 0) {
            throw new ShopException("the user with the given mailAddress does not exist");
        }


        int inventoryQuantity=inventoryManagementUseCases.getAvailableInventory(itemId)-
                getReservedInventoryInShoppingBaskets(itemId);
       

        if (inventoryQuantity<quantity) {
            throw new ShopException("the quantity not enough");
        }
        Optional<User> theUser = userRepository.findByMailAddress(userMailAddress);

        if (theUser.isEmpty()) {
            throw new ShopException("the user with the given mailAddress does not exist");
        }


        Optional<ShoppingBasket> shoppingBasket = shoppingBasketRepository.findByUserMailAddress(userMailAddress);

        AtomicBoolean addToBasket = new AtomicBoolean(false);
        if (shoppingBasket.isPresent()) {
            if (shoppingBasket.get().getItems() != null) {
                shoppingBasket.get().getItems().forEach((k, v) -> {
                    if (k.getId().equals(itemId)) {
                        shoppingBasket.get().getItems().replace(k, v + quantity);
                        addToBasket.set(true);

                    }
                });
                if (!addToBasket.get()) {
                    shoppingBasket.get().getItems().put(item.get(), quantity);
                    shoppingBasket.get().setShoppingBasketStatus(ShoppingBasketStatus.FILLED);
                }

                if (inventoryManagementUseCases.getAvailableInventory(itemId) < quantity) {
                    throw new ShopException("the quantity is not in stock");
                } else



                shoppingBasketRepository.save(shoppingBasket.get());

            }

        }
        else {
        HashMap<Item,Integer> map=new HashMap<>();
        map.put(item.get(),quantity);
        ShoppingBasket shoppingBasket1=new ShoppingBasket();
        shoppingBasket1.setUser(theUser.get());
        shoppingBasket1.setId(UUID.randomUUID());
        shoppingBasket1.setItems(map);
        shoppingBasket1.setShoppingBasketStatus( ShoppingBasketStatus.FILLED);

        shoppingBasketRepository.save(shoppingBasket1);}
        inventoryManagementUseCases.removeFromInventory(itemId,quantity);
    }

    @Override
    public void removeItemFromShoppingBasket(String userMailAddress, UUID itemId, int quantity) {
        if (userMailAddress == null || userMailAddress.isEmpty()) {
            throw new ShopException("select a emailaddress");
        }
        if (itemId == null || itemId.equals("")) {
            throw new ShopException("select a emailaddress");
        }
        Optional<Item> item = itemRepository.findById(itemId);
        if (item.isEmpty()||quantity < 0) {
            throw new ShopException("the user with the given mailAddress does not exist");
        }
        Optional<User> theUser = userRepository.findByMailAddress(userMailAddress);
        if (theUser.isEmpty()) {
            throw new ShopException("the user with the given mailAddress does not exist");
        }
        int itemQuantityInInventory = inventoryManagementUseCases.getAvailableInventory(itemId);
        if (itemQuantityInInventory <= 0) {
            throw new ShopException("item is not available in stock");
        }
        Optional<ShoppingBasket> shoppingBasket = shoppingBasketRepository.findByUserMailAddress(userMailAddress);
        if (shoppingBasket.isEmpty()) {
            throw new ShopException("the basket is not exist ");
        }
        if (getShoppingBasketAsMap(userMailAddress).get(itemId) == null)
            throw new ShopException("the item is not in the cart in the requested quantity");
        shoppingBasket.get().getItems().forEach((k, v) -> {
            if (k.getId().equals(itemId)) {
                if (v < quantity) {
                    throw new ShopException("the item is not in the cart in the requested quantity");
                }
                shoppingBasket.get().getItems().replace(k, v - quantity);
            }
        });
        if (shoppingBasket.get().getItems().get(item.get()) == 0) {
            shoppingBasket.get().getItems().remove(item.get());
        } if (shoppingBasket.get().getItems() == null) {
            shoppingBasket.get().setShoppingBasketStatus(ShoppingBasketStatus.EMPTY);
        }
        shoppingBasketRepository.save(shoppingBasket.get());

    }

    @Override
    public Map<UUID, Integer> getShoppingBasketAsMap(String userMailAddress) {
        if (userMailAddress == null || userMailAddress.isEmpty()) {
            throw new ShopException("select a emailaddress");
        }
        Optional<ShoppingBasket> shoppingBasket = shoppingBasketRepository.findByUserMailAddress(userMailAddress);
        if (shoppingBasket.isEmpty()) {
            throw new ShopException("the basket is not exist ");
        }
        Map<UUID, Integer> items=new HashMap<>();
        shoppingBasket.get().getItems().forEach((item,quantity)->{
            items.merge(item.getId(),quantity,Integer::sum);
        });

        return items;
    }

    @Override
    public float getShoppingBasketAsMoneyValue(String userMailAddress) {
        if (userMailAddress == null || userMailAddress.equals("")) {
            throw new ShopException("select a emailaddress");
        }
        Optional<User> theUser = userRepository.findByMailAddress(userMailAddress);
        if (theUser.isEmpty()) {
            throw new ShopException("the user with the given mailAddress does not exist");
        }
        Optional<ShoppingBasket> shoppingBasket = shoppingBasketRepository.findByUserMailAddress(userMailAddress);
        if (shoppingBasket.isEmpty())return 0;

        AtomicReference<Float> total= new AtomicReference<>((float) 0);
        shoppingBasket.get().getItems().forEach((item,quantity)->{
            total.updateAndGet(v -> new Float((float) (v + (item.getSalesPrice() * quantity))));
        });

        return total.get();

    }

    @Override
    public int getReservedInventoryInShoppingBaskets(UUID itemId) {
        if (itemId == null || itemId.equals("")) {
            throw new ShopException("select a emailaddress");
        }
        Optional<Item> item = itemRepository.findById(itemId);
        if (item.isEmpty()) return 0;
        List<ShoppingBasket> shoppingBaskets = shoppingBasketRepository.findAll();
        AtomicInteger quantity = new AtomicInteger();
        shoppingBaskets.forEach(b->{
            b.getItems().forEach((k, v) -> {
                if (k.getId().equals(itemId)) {
                    quantity.set(quantity.get() + v);

                }
            });
        });


        return quantity.get();

    }

    @Override
    public boolean isEmpty(String userMailAddress) {
        if (userMailAddress == null || userMailAddress.isEmpty()) {
            throw new ShopException("select a emailaddress");
        }
        Optional<User> theUser = userRepository.findByMailAddress(userMailAddress);
        if (theUser.isEmpty()) {
            throw new ShopException("the user with the given mailAddress does not exist");
        }
        Optional<ShoppingBasket> shoppingBasket = shoppingBasketRepository.findByUserMailAddress(userMailAddress);
        if (shoppingBasket.isPresent()) {
            if (shoppingBasket.get().getItems().isEmpty())
                return true;
            return false;
        }
        return true;
    }

    @Override
    public boolean isPaymentAuthorized(String userMailAddress) {
        if (userMailAddress == null || userMailAddress.isEmpty()) {
            throw new ShopException("a emailaddress of user is empty");
        }
        Optional<User> theUser = userRepository.findByMailAddress(userMailAddress);
        if (theUser.isEmpty()) {
            throw new ShopException("the user with the given mailAddress does not exist");
        }

           float basketValue=getShoppingBasketAsMoneyValue(userMailAddress);
float paymentAuthorized=paymentUseCases.getPaymentTotal(userMailAddress);
if(paymentAuthorized==0)return false;
        return basketValue==paymentAuthorized;
    }

    @Override
    public void checkout(String userMailAddress) {
        if (userMailAddress == null || userMailAddress.isEmpty()) {
            throw new ShopException("a emailaddress of user is empty");
        }



        Optional<User> theUser = userRepository.findByMailAddress(userMailAddress);
        if (theUser.isEmpty()) {
            throw new ShopException("the user with the given mailAddress does not exist");
        }
        Optional<ShoppingBasket> shoppingBasket = shoppingBasketRepository.findByUserMailAddress(userMailAddress);
        // Optional<Cart> cart=cartRepository.findCartByUser_MailAddress(userMailAddress);
        if (shoppingBasket.isEmpty() || getShoppingBasketAsMoneyValue(userMailAddress) == 0) {
            throw new ShopException("the basket is not exist ");
        }
        if (getShoppingBasketAsMap(userMailAddress).isEmpty()) {
            throw new ShopException("there is no Item in the Cart ");
        }

        paymentUseCases.authorizePayment(userMailAddress,getShoppingBasketAsMoneyValue(userMailAddress));
        if(isPaymentAuthorized(userMailAddress))
        shoppingBasket.get().setShoppingBasketStatus(ShoppingBasketStatus.PAYMENTAUTHORIZED);

        Delivery delivery = new Delivery();
        delivery.setName(theUser.get().getName());
        delivery.setUserMailAddress(theUser.get().getMailAddress());
        delivery.setStreet(theUser.get().getStreet());
        delivery.setCity(theUser.get().getCity());
        delivery.setPostalCode(theUser.get().getPostalCode());

        deliveryUseCases.triggerDelivery(delivery,getShoppingBasketAsMap(userMailAddress));
        shoppingBasket.get().setShoppingBasketStatus(ShoppingBasketStatus.DELEVERYTRIGGERED);

        Order order = new Order();
        order.setId(UUID.randomUUID());
        order.setUser(theUser.get());
        order.setOrderStatus(OrderStatus.CREATED);
        order.setItems(shoppingBasket.get().getItems());
        orderRepository.save(order);
        shoppingBasket.get().setShoppingBasketStatus(ShoppingBasketStatus.EMPTY);
        shoppingBasket.get().getItems().clear();
        shoppingBasketRepository.save(shoppingBasket.get());
    }

    @Override
    public Map<UUID, Integer> getOrderHistory(String userMailAddress) {
        if (userMailAddress == null || userMailAddress.isEmpty()) {
            throw new ShopException("select a emailaddress");
        }
        Optional<User> theUser = userRepository.findByMailAddress(userMailAddress);
        if (theUser.isEmpty()) {
            throw new ShopException("the user with the given mailAddress does not exist");
        }
         List<Order> orders=orderRepository.findAllByUserMailAddress(userMailAddress);


        Map<UUID, Integer> history = new HashMap<>();
        for (Order order:orders)
        order.getItems().forEach((item,quantity)->{
            history.merge(item.getId(),quantity,Integer::sum);
        });

        return history;
    }

    @Override
    public void deleteAllOrders() {
        shoppingBasketRepository.deleteAll();
        orderRepository.deleteAll();
    }


}
