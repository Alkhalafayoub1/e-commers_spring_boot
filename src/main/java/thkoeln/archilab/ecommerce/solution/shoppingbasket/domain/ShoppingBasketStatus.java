package thkoeln.archilab.ecommerce.solution.shoppingbasket.domain;

public enum ShoppingBasketStatus {
    EMPTY,
    FILLED,
    PAYMENTAUTHORIZED,
    DELETED,
    DELEVERYTRIGGERED
}
