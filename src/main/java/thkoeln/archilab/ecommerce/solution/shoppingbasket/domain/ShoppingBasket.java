package thkoeln.archilab.ecommerce.solution.shoppingbasket.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import thkoeln.archilab.ecommerce.solution.item.domain.Item;
import thkoeln.archilab.ecommerce.solution.user.domain.User;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShoppingBasket {
    @Id
    private UUID id=UUID.randomUUID();
    @OneToOne(cascade = CascadeType.ALL)
    private User user;
    // @ElementCollection
    // private List<ItemLine> itemLines=new ArrayList<>();
    @ElementCollection
    private Map<Item, Integer> items = new HashMap<>();
    private ShoppingBasketStatus shoppingBasketStatus;


}
