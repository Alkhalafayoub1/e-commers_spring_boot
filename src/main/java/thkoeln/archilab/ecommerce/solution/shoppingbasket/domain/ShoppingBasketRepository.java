package thkoeln.archilab.ecommerce.solution.shoppingbasket.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import thkoeln.archilab.ecommerce.solution.user.domain.User;

import java.util.Optional;
import java.util.UUID;

public interface ShoppingBasketRepository extends JpaRepository<ShoppingBasket, UUID> {
    Optional<ShoppingBasket> findByUserMailAddress(String email);

    Optional<ShoppingBasket> findByUser(User user);
}
